# AssetLibrary

Creates assets from a given file and a list of actions.

Refer to the Wiki for:

* [Installation](https://bitbucket.org/scooblyboo/assetlibrary/wiki/Installation)
* [Testing](https://bitbucket.org/scooblyboo/assetlibrary/wiki/Testing)
* [Usage](https://bitbucket.org/scooblyboo/assetlibrary/wiki/Usage)